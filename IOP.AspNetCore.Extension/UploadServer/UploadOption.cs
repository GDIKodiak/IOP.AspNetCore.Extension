﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace IOP.AspNetCore.Extension.UploadServer
{
    /// <summary>
    /// 上传选项
    /// </summary>
    public class UploadOption
    {

        /// <summary>
        /// 文件上传地址
        /// </summary>
        public string UploadUrl { get; set; } = "/api/file/Upload";

        /// <summary>
        /// 物理路径
        /// </summary>
        public string PhysicPath { get; set; } = "";

        /// <summary>
        /// 文件请求路径
        /// </summary>
        public string FileRequestPath { get; set; } = "/api/file";

        /// <summary>
        /// 自定义文件处理
        /// </summary>
        public Action<HttpContext> CustomFileHandle { get; set; } = null;

        /// <summary>
        /// MIME映射
        /// </summary>
        public Dictionary<string, string> MIMEMapping { get; set; } = new Dictionary<string, string>()
        {
            { ".jpg",MediaTypeNames.Image.Jpeg },
            { ".png", "image/png"},
            { ".mp4", "video/mp4" },
            { ".html", MediaTypeNames.Text.Html },
            { ".xml", MediaTypeNames.Text.Xml }
        };
    }
}
