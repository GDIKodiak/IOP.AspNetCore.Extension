﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;

namespace IOP.AspNetCore.Extension.Auth
{
    /// <summary>
    /// 令牌配置类
    /// </summary>
    public class TokenOptions : IOptions<TokenOptions>
    {
        /// <summary>
        /// 发行者
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// 订阅者
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public TimeSpan Expiration { get; set; } = TimeSpan.FromMinutes(5000);
        /// <summary>
        /// 签名证书
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; } = null;

        public TokenOptions Value => this;
    }
}
