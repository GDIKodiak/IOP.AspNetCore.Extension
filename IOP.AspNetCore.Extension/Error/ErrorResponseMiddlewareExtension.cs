﻿using Microsoft.AspNetCore.Builder;

namespace IOP.AspNetCore.Extension.Error
{
    /// <summary>
    /// 错误回执中间件
    /// </summary>
    public static class ErrorResponseMiddlewareExtension
    {
        /// <summary>
        /// 使用错误回执中间件
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseErrorMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorResponseMiddleware>();
        }
    }
}
