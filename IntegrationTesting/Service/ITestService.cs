﻿using IOP.AspNetCore.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationTesting.Service
{
    public interface ITestService : IAPIService
    {
        string Test();
    }
}
