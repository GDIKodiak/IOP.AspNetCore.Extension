﻿using System.IO;
using IOP.AspNetCore.Extension;
using IOP.AspNetCore.Extension.UploadServer;
using IOP.AspNetCore.Extension.Auth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System;
using IOP.AspNetCore.Extension.Swagger;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace IntegrationTesting
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddAPIServices();

            #region 认证
            //services.AddJWTAuthentication(Configuration);
            var audienceConfig = Configuration.GetSection("JWTAuthentication") ?? throw new ArgumentNullException("Audience is Null in appsettings.json");
            var symmetricKeyAsBase64 = audienceConfig["Secret"] ?? throw new NullReferenceException("Secret is Null in appsettings.json");
            var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
            var signingKey = new SymmetricSecurityKey(keyByteArray);
            services.AddJWTAuthentication(option =>
            {
                option.Audience = "test";
                option.Issuer = "test";
                option.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
                option.Expiration = TimeSpan.FromMinutes(5000);

            });
            #endregion
            #region Swagger服务
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Test API",
                    Description = "Test",
                    TermsOfService = "None",
                });

                // Set the comments path for the Swagger JSON and UI.
                //var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
                c.OperationFilter<HttpHeaderOperation>();
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            #region Swaager中间件
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");
            });
            #endregion

            app.UseUploadServer(option =>
            {
                option.PhysicPath = Path.Combine(env.ContentRootPath, "File");
            });



            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
